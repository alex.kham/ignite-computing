package ru.khamitov.computing;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.khamitov.computing.service.FileProcessor;

@SpringBootApplication
public class Application implements ApplicationRunner {

    @Autowired
    FileProcessor fileProcessor;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        String[] sourceArgs = args.getSourceArgs();
        if (sourceArgs.length != 1) {
            throw new IllegalArgumentException();
        }
        System.out.println(fileProcessor.process(sourceArgs[0]));
    }
}

package ru.khamitov.computing.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.scripting.ScriptEvaluator;
import org.springframework.scripting.ScriptSource;
import org.springframework.scripting.groovy.GroovyScriptEvaluator;
import org.springframework.scripting.support.ResourceScriptSource;

@Configuration
public class ScriptEvaluatorConfig {

    @Value("${application.script.f1}")
    private Resource f1;
    @Value("${application.script.f2}")
    private Resource f2;

    @Bean
    public ScriptEvaluator groovyScriptEvaluator() {
        return new GroovyScriptEvaluator();
    }

    @Bean
    public ScriptSource f1() {
        return new ResourceScriptSource(this.f1);
    }

    @Bean
    public ScriptSource f2() {
        return new ResourceScriptSource(this.f2);
    }

}

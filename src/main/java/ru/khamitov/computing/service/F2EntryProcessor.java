package ru.khamitov.computing.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.khamitov.computing.service.calculator.Calculator;

import javax.cache.processor.EntryProcessor;
import javax.cache.processor.EntryProcessorException;
import javax.cache.processor.MutableEntry;

@Service
public class F2EntryProcessor implements EntryProcessor<String, Long, Long> {

    @Autowired
    private Calculator calculator;

    @Override
    public Long process(MutableEntry<String, Long> mutableEntry, Object... objects) throws EntryProcessorException {

        Long arg = (Long) objects[0];
        Long newValue = mutableEntry.exists() ? calculator.f2(mutableEntry.getValue(), arg)
                : calculator.f2(0L, arg);
        mutableEntry.setValue(newValue);

        return newValue;
    }
}

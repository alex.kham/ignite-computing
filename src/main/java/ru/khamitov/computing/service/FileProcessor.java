package ru.khamitov.computing.service;

import java.io.IOException;

public interface FileProcessor {

    Long process(String path) throws IOException;
}

package ru.khamitov.computing.service;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.lang.IgniteClosure;
import org.apache.ignite.lang.IgniteFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.khamitov.computing.service.calculator.Calculator;

import javax.cache.processor.EntryProcessor;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Service
public class FileProcessorImpl implements FileProcessor {

    private static final String SEPARATOR = " ";

    @Autowired
    Ignite igniteClient;
    @Autowired
    Calculator calculator;
    @Autowired
    EntryProcessor<String, Long, Long> f2EntryProcessor;

    @Override
    public Long process(String path) throws IOException {
        String resultKey = "result";
        IgniteCache<String, Long> resultStore = igniteClient.cache("computing");
        List<IgniteFuture<Long>> igniteFutures = new ArrayList<>();

        try (Stream<String> fileStream = Files.lines(Paths.get(path))) {
            fileStream.forEach(line -> {
                IgniteFuture<Long> igniteFuture = igniteClient.compute().applyAsync(
                        (IgniteClosure<String, Long>) s -> {
                            String[] numbers = s.split(SEPARATOR);
                            Long f1Result = calculator.f1(Long.valueOf(numbers[0]), Long.valueOf(numbers[1]));
                            return resultStore.invoke(resultKey, f2EntryProcessor, f1Result);
                        }, line);

                igniteFutures.add(igniteFuture);
            });
        }

        igniteFutures.stream().forEach(IgniteFuture::get);
        Long result = resultStore.get(resultKey);
        igniteClient.close();

        return result;
    }



}

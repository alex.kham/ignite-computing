package ru.khamitov.computing.service.calculator;

public interface Calculator {
   Long f1(Long x, Long y);
   Long f2(Long x, Long y);
}

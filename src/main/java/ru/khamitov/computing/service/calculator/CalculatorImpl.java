package ru.khamitov.computing.service.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scripting.ScriptEvaluator;
import org.springframework.scripting.ScriptSource;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CalculatorImpl implements Calculator {

    @Autowired
    ScriptEvaluator scriptEvaluator;
    @Autowired
    @Qualifier("f1")
    ScriptSource f1;
    @Autowired
    @Qualifier("f2")
    ScriptSource f2;

    @Override
    public Long f1(Long x, Long y) {
        return calculate(x, y, f1);
    }

    @Override
    public Long f2(Long x, Long y) {
        return calculate(x, y, f2);
    }

    public Long calculate(Long x, Long y, ScriptSource scriptSource) {
        Map<String, Object> params = new HashMap<>();
        params.put("x", x);
        params.put("y", y);
        return (long) this.scriptEvaluator.evaluate(scriptSource, params);
    }
}

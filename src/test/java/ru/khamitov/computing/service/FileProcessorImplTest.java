package ru.khamitov.computing.service;

import junit.framework.TestCase;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.khamitov.computing.config.IgniteConfig;
import ru.khamitov.computing.config.ScriptEvaluatorConfig;
import ru.khamitov.computing.service.calculator.CalculatorImpl;

import javax.cache.processor.EntryProcessor;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {IgniteConfig.class, ScriptEvaluatorConfig.class,
        FileProcessorImpl.class, CalculatorImpl.class, F2EntryProcessor.class})
public class FileProcessorImplTest extends TestCase {
    @Autowired
    FileProcessor fileProcessor;
    static final String TEST_FILE = "/test.txt";
    @Autowired
    Ignite igniteClient;
    @Autowired
    EntryProcessor<String, Long, Long> f2EntryProcessor;

    @BeforeClass
    public static void startUp() {
        Ignition.start("ignite-server.xml");
    }

    @Test
    public void testProcess() throws InterruptedException, IOException, URISyntaxException {
        Long process = fileProcessor.process(Paths.get(getClass().getResource(TEST_FILE).toURI()).toString());
        assertEquals(Long.valueOf(1_584), process);
    }

    @Test
    public void testEntryProcessor() throws Exception {
        int THREADS = 20;
        ExecutorService executorService = Executors.newFixedThreadPool(THREADS);
        String resultKey = "resultProcessor";
        CountDownLatch countDownLatch = new CountDownLatch(THREADS);
        IgniteCache<String, Long> resultStore = igniteClient.cache("computing");
        IntStream.range(0, THREADS).forEach((i) -> {
            executorService.submit(() -> {
                resultStore.invoke(resultKey, f2EntryProcessor, 1L);
                countDownLatch.countDown();
            });
        });
        countDownLatch.await();
        assertEquals(Long.valueOf(20), resultStore.get(resultKey));
    }
}

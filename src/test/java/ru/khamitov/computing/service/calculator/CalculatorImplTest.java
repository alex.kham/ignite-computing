package ru.khamitov.computing.service.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.khamitov.computing.config.ScriptEvaluatorConfig;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {ScriptEvaluatorConfig.class,CalculatorImpl.class})
public class CalculatorImplTest{

    @Autowired
    Calculator calculator;

    @Test
    public void testF1() {
        assertEquals(Long.valueOf(55), calculator.f1(66L, 11L));
    }

    @Test
    public void testF2() {
        assertEquals(Long.valueOf(35), calculator.f2(30L, 5L));
    }
}
